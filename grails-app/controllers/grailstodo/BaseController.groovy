package grailstodo


import grails.rest.*
import grails.converters.*

class BaseController {
	static responseFormats = ['json', 'xml']
	
    def index() { }

    def successResponse(message,data) {
        return [
                "status":"success",
                "message":message,
                "data":data
        ]
    }
}
