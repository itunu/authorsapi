package grailstodo

import grails.validation.ValidationException
import org.grails.core.exceptions.GrailsException
import org.springframework.dao.DataIntegrityViolationException

import static org.springframework.http.HttpStatus.*
class AuthorController extends BaseController {

    AuthorService  authorService
    BookService    bookService
	static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
    def index() {
        respond authorService.listAuthors()
    }

    def save() {
        try {
            def requestParams = request.JSON
            def author = authorService.save(requestParams)
            if(author) {
               respond  this.successResponse('Author saved successfully', author)
            }
        } catch (ValidationException e) {
            respond e.getMessage()
        }
    }

    def addBook() {
        try {
            def requestParams = request.JSON
            def book = bookService.saveBook(requestParams)
            if(book) {
                def response = authorService.addBook(book, params.id);
                if(response) {
                    respond this.successResponse('Book added successfully', response)
                }
            }
        } catch (ValidationException e) {
            respond e.getMessage()
        }
    }

    def update() {
        try {
            def requestParams = request.JSON
            def response = authorService.update(requestParams, params.id)
            if(response) {
                respond this.successResponse('Author updated successfully', response)
            }
        } catch (ValidationException e) {
            respond e.getMessage()
        }
    }

    def delete() {
        try {
             authorService.delete(params.id)
             respond this.successResponse('Author deleted successfully', [])
        } catch (DataIntegrityViolationException e) {
            respond e.getMessage()
        }
    }
}
