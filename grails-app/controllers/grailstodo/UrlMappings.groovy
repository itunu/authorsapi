package grailstodo

class UrlMappings {

    static mappings = {
        get "/authors"(controller: "author", action: "index")
        post "/authors"(controller:"author", action: "save")
        post "/authors/$id/book"(controller:"author", action: "addBook")
        put "/authors/$id/update"(controller:"author", action: "update")
        delete "/authors/$id"(controller:"author", action: "delete")

        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
