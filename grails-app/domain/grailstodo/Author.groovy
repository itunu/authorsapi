package grailstodo
import  grails.rest.*

@Resource(uri ='/authors')
class Author {
    String name
    Integer age
    String  dob
    List    books
    static hasMany = [books: Book]


    static constraints = {
        name blank: false
        age blank: false
        dob blank : false
    }
}
