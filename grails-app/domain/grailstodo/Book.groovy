package grailstodo

class Book {
    String title
    String isbn
    String publisher
    String date_published
    static belongsTo = [author: Author]

    static constraints = {
        title blank: false
        isbn  blank: false
        publisher blank: false
        date_published blank: false
    }
}
