package grailstodo

import grails.gorm.transactions.Transactional

@Transactional
class BookService {

    def listBooks() {
        return   Book.list()
    }

    def saveBook(request) {
        def newBook = new Book()
        newBook.properties = request
        return newBook
    }
}
