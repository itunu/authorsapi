package grailstodo

import grails.gorm.transactions.Transactional

@Transactional
class AuthorService {

    @Transactional(readOnly =true )
    def listAuthors() {
      return   Author.list(offset:0, max:10)
    }

    def save(requestParams) {
        def newAuthor = new Author()
        newAuthor.properties = requestParams
        newAuthor.save()
        return newAuthor
    }

    def addBook(book, id) {
        def author = Author.get(id)
        if(author) {
            author.addToBooks(book)
            author.save()
            return author
        }
    }

    def update(updates, id) {
        def author = Author.get(id)
        if(author) {
          updates.every{
              it -> author[it.key] = updates[it.key]
          }
         author.save()
         return author
        }
    }

    def delete(id) {
        def author = Author.get(id)
        author.delete(flush: true)
    }
}
